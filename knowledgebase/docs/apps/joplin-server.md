# <img src="/img/joplin-server-logo.png" width="25px"> Joplin Server App

## About

Joplin is a free, open source note taking and to-do application, which can handle a large number of notes organised into notebooks.

* [Website](https://joplinapp.org/)
* [Issue tracker](https://github.com/laurent22/joplin/issues)

## Clients

Download the clients [here](https://joplinapp.org/download/).

