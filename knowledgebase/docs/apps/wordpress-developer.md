# <img src="/img/wordpress-developer-logo.png" width="25px"> WordPress (Developer) App

## About

This app is targeted at users who want to have complete control over their WordPress installation.
The WordPress code can be accessed and edited via SFTP. WordPress' built-in updater has to be used
to periodically check and install updates. If you prefer delegating the responsibility of applying
updates to the Cloudron team, use the [WordPress Managed app](/apps/wordpress-managed) instead.

This package also supports WordPress Multisite.

* Questions? Ask in the [Cloudron Forum - WordPress (Developer)](https://forum.cloudron.io/category/77/wordpress-developer)

## Admin page

The WordPress admin page is located `https://<my.example.com>/wp-login.php`.

## Using SFTP

The app can be uploaded using an SFTP client like [FileZilla](https://filezilla-project.org/).

You can find the SFTP login details when clicking on the `i` icon in the app grid.

<center>
<img src="/img/lamp-filezilla.png" class="shadow">
</center>

!!! note "SFTP Access"
    SFTP access for non-admin users can be granted using the [access control UI](/apps/#restricting-app-access-to-specific-users).

## Memory limits

To adjust memory allocated for WordPress, edit `/app/data/wp-config.php` using the [File manager](/apps#file-manager)
and add the following line at the end of the file:

```
define('WP_MEMORY_LIMIT', '128M');
define('WP_MAX_MEMORY_LIMIT', '256M');
```

Note that the app also has a separate memory limit controlled by the app's [memory limit](/apps/#increasing-the-memory-limit-of-an-app). If you increase `WP_MEMORY_LIMIT`, be sure to increase the app's memory limit. A good formula is to provide the app 6 times the `WP_MEMORY_LIMIT` value at the bare minimum.

`WP_MAX_MEMORY_LIMIT` is the limit for administration tasks, which often require more.

A detailed explanation can be found in the [WordPress docs](https://wordpress.org/support/article/editing-wp-config-php/#increasing-memory-allocated-to-php).

## htaccess

By default the app does not have an `.htaccess` file. It can be added via [SFTP](/apps/wordpress-Developer/#using-sftp) or the [File manager](/apps#file-manager) into the app at `/app/data/public/.htaccess` for `/` or depending on where it is required in any of the other WordPress related subfolders in `/app/data/public/`.

## Cron tasks

The app is configured to run WordPress cron tasks every minute.

To run the cron tasks manually run the following command using the
[Web terminal](/apps#web-terminal):

```
wp cron event run --due-now
```

WordPress' built-in cron task schedule `wp-cron` is disabled since
it is [not effective](https://www.lucasrolff.com/wordpress/why-wp-cron-sucks/)
for low traffic websites.

To add custom cron events, use a plugin like [WP Crontrol](https://wordpress.org/plugins/wp-crontrol/).

## Plugins

Unlike the [Managed WordPress app](/apps/wordpress-managed), you can install
plugins that modify the code.

## Performance

[GTmetrix](https://gtmetrix.com) is a great site for getting performance metrics on the
WordPress installation.

* To set the expires headers for all pages, the [WP Fastest Cache](https://wordpress.org/plugins/wp-fastest-cache/)
  plugin can be installed.

* For CDN caching, we recommend [WP Fastest Cache](https://wordpress.org/plugins/wp-fastest-cache/) or
[W3 Total Cache](https://wordpress.org/plugins/w3-total-cache/) for CDN based cache. Ryan Kite has a
[good tutorial](https://ryan-kite.com/how-to-create-a-cdn-for-wp-fastest-cache-with-aws-cloudfront/) on
how to setup AWS Cloudfront with WP Fastest Cache.

## Database access

Cloudron does not support PHPMyAdmin. It is, however, possible to access the database
using other methods:

* Open a [Web terminal](/apps#web-terminal) and press the 'MySQL' button to get console
  access. You can execute SQL commands directly.

* Use a plugin like [WP phpMyAdmin](https://wordpress.org/plugins/wp-phpmyadmin-extension/) or 
  [ARI Adminer](https://wordpress.org/plugins/ari-adminer/) for a GUI.

## WP CLI

[WP CLI](http://wp-cli.org/) is a command line interface to WordPress. To run commands
using the CLI tool, open a [Web terminal](/apps#web-terminal) and
execute commands WP CLI using simply `wp`. It is pre-setup to run as the correct user already.
For example:

```
wp user list
```

If one or more plugins/themes are erroring, you can make WP CLI skip loading plugins/themes as follows:

```
wp --skip-plugins --skip-themes
```

Additional php settings can be configured, when running the cli manually with `php -d key=value`:

```
sudo -E -u www-data php -d max_execution_time=100 /app/pkg/wp --path=/app/data/public/
```

In this case setting the maximum execution timeout to 100 seconds.

## PHP settings

You can add custom [PHP settings](http://php.net/manual/en/ini.core.php) in `/app/data/php.ini`

### File upload size

Change the following values in `/app/data/php.ini`:

```
post_max_size = 256M
upload_max_filesize = 256M
memory_limit = 256M
```

## Migrating existing site

See our [blog](https://blog.cloudron.io/migrating-a-wordpress-site-to-cloudron/) on how to migrate an existing
WordPress site to Cloudron.

## File editing

WordPress' built-in file editing functionality is enabled by default. For security reasons, we recommend that
you turn this off by editing `/app/data/wp-config.php` and setting `DISALLOW_FILE_EDIT` to true.

```
define('DISALLOW_FILE_EDIT', true);
```

## Unfiltered HTML

Non-admins are allowed to post unfiltered HTML content. You can disable this by editing
`/app/data/wp-config.php` and setting `DISALLOW_UNFILTERED_HTML` to true.


```
define('DISALLOW_UNFILTERED_HTML', true);
```

## Multisite

!!! note "To multisite or not to multisite"
    WordPress multisite is a complex system with many compatibility gotchas. Unless you have a strong reason, we recommend
    installing a separate WordPress app for each site.

To enable WordPress multisite, start with a fresh installation. There are two way to convert a fresh installation to multisite
- using the WP CLI tool or using the WordPress Network Setup Tool.


### CLI

You can install WordPress multisite using the CLI tool as follows:

* Open a [Web Terminal](/apps/#web-terminal). To use subdomains in multisite mode:

```
# /app/pkg/wp-convert-multisite --subdomains
```

You can pass `--subdirectories` to install in subdirectory mode.

The command will convert existing site to multisite and also place the required `.htaccess` rules for multisite to work.

* When using subdomains, go to the `Location` view of the Cloudron dashboard and configure a Wildcard alias.

<center>
<img src="/img/wordpress-multisite-alias.png" class="shadow">
</center>

* That's it! You can add new sites from the `Network Admin` menu. You have to always add a site as a subdomain (or subdirectory). The
Site Address can be changed after addition by editing the site. If you set the Site Address to a different domain, you simply have to add
it to the domain aliases in the `Location` section in Cloudron Dashboard.

<center>
<img src="/img/wordpress-multisite-new-site.png" class="shadow">
</center>

### Network Setup Tool

You can install WordPress multisite using WordPress' Network Setup tool as follows:

* Enable Multisite in `/app/data/public/wp-config.php` by adding the following line using the [File manager](/apps/#file-manager).
Add this line above the line that says "That’s all, stop editing! Happy blogging.":

```
/* Multisite */
define( 'WP_ALLOW_MULTISITE', true );
```

* Run through the WordPress Network Setup in `Tools` -> `Network Setup` in the WordPress dashboard. You might have to refresh the
browser page for this to appear. As instructed in that page, deactivate all the plugins before proceeding. Cloudron supports both sub-domain
and sub-directory installation.

<center>
<img src="/img/wordpress-tools-network-setup.png" class="shadow">
</center>

* Once you click install, you will see a message `Warning! Wildcard DNS may not be configured correctly!`. To fix this, go to the
`Location` view of the Cloudron dashboard and configure a Wildcard alias. Once the alias has been added, the warning will disappear
(you have to refresh the WordPress dashboard).

<center>
<img src="/img/wordpress-multisite-alias.png" class="shadow">
</center>


* To complete the network installation, add the following to `/app/data/public/wp-config.php` as instructed.

```
define('MULTISITE', true);
define('SUBDOMAIN_INSTALL', true);
define('DOMAIN_CURRENT_SITE', 'msite.cloudron.club');
define('PATH_CURRENT_SITE', '/');
define('SITE_ID_CURRENT_SITE', 1);
define('BLOG_ID_CURRENT_SITE', 1);
```

Also, completely replace the contents of `/app/data/public/.htaccess` as instructed. Note that the Rewrite rules are slightly different
for sub-domain and sub-directory setups. The config below is for sub-domain setup:

```
RewriteEngine On
RewriteRule .* - [E=HTTP_AUTHORIZATION:%{HTTP:Authorization}]
RewriteBase /
RewriteRule ^index\.php$ - [L]

# add a trailing slash to /wp-admin
RewriteRule ^wp-admin$ wp-admin/ [R=301,L]

RewriteCond %{REQUEST_FILENAME} -f [OR]
RewriteCond %{REQUEST_FILENAME} -d
RewriteRule ^ - [L]
RewriteRule ^(wp-(content|admin|includes).*) $1 [L]
RewriteRule ^(.*\.php)$ $1 [L]
RewriteRule . index.php [L]
```

* That's it! You can add new sites from the `Network Admin` menu. You have to always add a site as a subdomain (or subdirectory). The
Site Address can be changed after addition by editing the site. If you set the Site Address to a different domain, you simply have to add
it to the domain aliases in the `Location` section in Cloudron Dashboard.

<center>
<img src="/img/wordpress-multisite-new-site.png" class="shadow">
</center>

### Email Setup (Multisite)

In multisite mode, the SMTP mailer plugin can be configured per-site. After adding a site, restart the app for the package code to
automatically configure the plugin.

