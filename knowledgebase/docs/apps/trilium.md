# <img src="/img/trilium-logo.png" width="25px"> Trilium App

## About

Trilium Notes is a hierarchical note taking application with focus on building large personal knowledge bases.

* Questions? Ask in the [Cloudron Forum - Trilium Notes](https://forum.cloudron.io/category/95/trilium)
* [Trilium Notes Website](https://github.com/zadam/trilium)
* [Trilium Notes issue tracker](https://github.com/zadam/trilium/issues)

## Multiple users

Trilium does not support multiple users. It's a single user application intended
for personal notes. See the [FAQ](https://github.com/zadam/trilium/wiki/FAQ#multi-user-support)
for more information.

