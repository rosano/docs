# <img src="/img/confluence-logo.png" width="25px"> Confluence App

## About

Confluence is purpose-built for teams that need a secure and reliable way to collaborate on mission-critical projects.

* Questions? Ask in the [Cloudron Forum - Confluence](https://forum.cloudron.io/category/16/confluence)
* [Confluence Website](https://www.atlassian.com/software/confluence)
* [Confluence forum](https://community.atlassian.com/)

## Completing the installation

To finish the installation, do the following:

* Select Production Installation
* Add your license key.
* Select `PostgreSQL` as External Database.
* Choose `By connection string`.
* Use the [File Manager](/apps/#file-manager) and open `/app/data/credentials.txt` to get database settings.

## Adminstration check list

* Disable `Backup Confluence` Schedule Jobs. The Cloudron backups up confluence already.

* To enable LDAP, go to `Confluence Administration`:

    * `Users & Security` -> `User directories`
    * `Add Directory` -> `Internal with LDAP authentication`
    * Use the [File Manager](/apps/#file-manager) and open `/app/data/credentials.txt` to get LDAP settings.
    * You can make Cloudron users admin once they log in to Confluence and appear in user listing via LDAP

* To configure mail:

    * `Mail Servers` -> `Add SMTP mail`
    * Use the [File Manager](/apps/#file-manager) and open `/app/data/credentials.txt` to get LDAP settings.

## Oracle Java

OpenJDK is [not supported](https://confluence.atlassian.com/confkb/is-openjdk-supported-by-confluence-297667642.html)
by Confluence. For this reason, the Cloudron app uses Oracle Java.

