# <img src="/img/radicale-logo.png" width="25px"> Radicale App

## About

Radicale is a small but powerful CalDAV (calendars, to-do lists) and CardDAV (contacts) server.

* Questions? Ask in the [Cloudron Forum - Radicale](https://forum.cloudron.io/category/76/radicale)
* [Radicale Website](http://radicale.org/)
* [Radicale issue tracker](https://github.com/Kozea/Radicale/issues)

## Custom permissions (e.g. shared calendar)

Per default, each user can only access their own calendar and contacts. If you
want something more complicated you can change the permissions.

You can change the permissions editing `/app/data/rights` using the [File Manager](/apps/#file-manager)
in the app instance. The default content of that file is:

    [owner-write]
    user = .+
    collection = %(login)s(/.*)?
    permission = rw

    [read]
    user = .*
    collection =
    permission = r

You can extend the file using the syntax described in [the radicale documentation](https://radicale.org/3.0.html#documentation/authentication-and-rights).

