# <img src="/img/ackee-logo.png" width="25px"> Ackee App

## About

Ackee is a Self-hosted, Node.js based analytics tool for those who care about privacy. 

* Questions? Ask in the [Cloudron Forum - CouchPotato](https://forum.cloudron.io/category/118/couchpotato)
* [Ackee Website](https://ackee.electerious.com/)
* [Ackee docs](https://docs.ackee.electerious.com/)
* [Ackee issue tracker](https://github.com/electerious/Ackee/issues)

## Admin Password

The admin password can be changed by editing `/app/data/env` using the [File manager](/apps/#file-manager). Be
sure to restart the app after making the change.

## Adding a domain

* First, add a domain inside Ackee's Settings page. If your website is `www.cloudron.space`, add it like so:

  <center>
  <img src="/img/ackee-add-domain.png" class="shadow">
  </center>

* Next, add [CORS configuration](https://docs.ackee.electerious.com/#/docs/CORS%20headers#platforms-as-a-service-configuration) by
  editing `/app/data/env` using the [File manager](/apps/#file-manager) and adding your website in `ACKEE_ALLOW_ORIGIN`.

```
ACKEE_ALLOW_ORIGIN="https://www.cloudron.space"
```

* Restart Ackee after making the above change.

* Embed Ackee's tracker.js script in your website. Be sure to replace the ackee URL and the domain ID. You can get the snippet below by
  clicking on the domain entry in Ackee's setting view as well.

```
<script async src="https://analytics.cloudron.space/tracker.js" data-ackee-server="https://analytics.cloudron.space" data-ackee-domain-id="217118b9-1843-4462-82ba-2e0acd189b91"></script>
```

  <center>
  <img src="/img/ackee-embed-script.png" class="shadow">
  </center>

## Data collection

By default, Ackee won't track personal information like device and browser info. To enable detailed tracking, pass the `data-ackee-opts` to the script tag:

```
<script async src="https://analytics.cloudron.space/tracker.js" data-ackee-server="https://analytics.cloudron.space" data-ackee-domain-id="217118b9-1843-4462-82ba-2e0acd189b91" data-ackee-opts='{ "ignoreLocalhost": true, "detailed": true }'></script>
```

You should now be able to see detailed stats:

  <center>
  <img src="/img/ackee-detailed-stats.png" class="shadow">
  </center>

See Ackee's [docs](https://github.com/electerious/Ackee/blob/master/docs/Anonymization.md#personal-data) for more information.

