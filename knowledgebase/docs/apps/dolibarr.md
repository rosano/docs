# <img src="/img/dolibarr-logo.png" width="25px"> Dolibarr App


## About

Dolibarr is an open source ERP & CRM for business.

* Questions? Ask in the [Cloudron Forum - Dolibarr](https://forum.cloudron.io/category/102/dolibarr)
* [Dolibarr Website](https://dolibarr.org/)
* [Dolibarr forum](https://www.dolibarr.org/forum.php)
* [Dolibarr issue tracker](https://github.com/Dolibarr/dolibarr/issues)

