# <img src="/img/wikijs-logo.png" style="height: 48px; vertical-align: middle;"> Wiki.js App

## About

Wiki.js is a wiki engine running on Node.js

* Questions? Ask in the [Cloudron Forum - Wiki.js](https://forum.cloudron.io/category/51/wiki-js)
* [Wiki.js Website](https://wiki.js.org/)
* [Wiki.js issue tracker](https://github.com/Requarks/wiki/issues)

## Git Storage

Wiki.js supports storing documentation in git storage using the [Git module](https://docs.requarks.io/en/storage/git).

* To get started, open a [Web Terminal](/apps/#web-terminal) and generate the keys in `/app/data/ssh/id_rsa` (see below).
Be sure to leave the passphrase empty as required by Wiki.js.

```
# ssh-keygen -t rsa -b 4096
Generating public/private rsa key pair.
Enter file in which to save the key (/root/.ssh/id_rsa): /app/data/ssh/id_rsa
Enter passphrase (empty for no passphrase): 
Enter same passphrase again: 
Your identification has been saved in /app/data/ssh/id_rsa
Your public key has been saved in /app/data/ssh/id_rsa.pub
...
```

* Create the repo directory and fix permissions

```
# mkdir -p /app/data/repo
# chown -R cloudron:cloudron /app/data/repo /app/data/ssh
```

* Use the following settings when configuring Wiki.js git storage:
    * Set the `SSH Private Key Mode` to `path`
    * Set `SSH Private Key Path` to `/app/data/ssh/id_rsa` (generated above)
    * Set the Local Repository Path to `/app/data/repo` (created above)
    * Newer GitHub repositories use the branch name as `main` instead of `master`. Use the appropriate branch name in Wiki.js config page.
