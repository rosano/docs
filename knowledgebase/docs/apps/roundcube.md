# <img src="/img/roundcube-logo.png" width="25px"> Roundcube App

## About

Roundcube webmail is a browser-based multilingual IMAP client with an application-like user interface.

* Questions? Ask in the [Cloudron Forum - Roundcube](https://forum.cloudron.io/category/22/roundcube)
* [Roundcube Website](https://roundcube.net)
* [Upstream Roundcube issue tracker](https://github.com/roundcube/roundcubemail/issues)

## Default Setup

Roundcube is pre-configured for use with Cloudron Email.

## Multi-domain Setup

Users can login with their email and password to access their mailbox. If the
Cloudron has two domains, `example1.com` and `example2.com`, the user can login
using `user@example1.com` and `user@example2.com`. Aliases can be added as identities
under Roundcube settings.

## External domains

The roundcube app does not support adding domains that are not managed in Cloudron.
Consider using [Rainloop app](/apps/rainloop) as an alternative.

## Vacation Email

An out of office / vacation mail message can be setup using Sieve filters.

A vacation message can be set in `Settings` -> `Filters` -> `Add filter` -> `Vacation message` action.

<center>
<img src="/img/email-vacation-message-roundcube.png" class="shadow" width="600px">
</center>

## Forwarding all emails

To forward all emails to an external mail, setup a Sieve filter in
`Settings` -> `Filters` -> `Add a filter` -> `Forward to`

<center>
<img src="/img/forward-all-emails-roundcube.png" class="shadow" width="600px">
</center>

## Plugins

[Plugins](https://packagist.org/?type=roundcube-plugin) can be installed from
plugin release tarballs. Cloudron package does not support installing plugins via
Composer since that involves editing `composer.json` which makes automatic updates
tricky.

* Upload the release tarball of the plugin using the [File Manager](/apps#file-manager) into
  `/app/data/plugins` and extract it. After extract, you might need to prettify the directory name. For example,
  rename `roundcube-contextmenu-3.3` to just `contextmenu`. Make a note of this directory name since
  it required below.

* Change the ownership of the extracted plugin to `www-data` using the [File Manager](/apps#file-manager).

* Add the plugin to `$config['plugins']` in `/app/data/customconfig.php`. The name below must match the
  name of the directory in the first step.

```
    array_push($config['plugins'], 'myplugin');
```

* Some plugin release tarballs do not contain dependancies and have to be installed via `composer`.
  `composer` requires a lot of RAM for it's resolution mechanism. For this reason, first bump the
  [memory limit](/apps/#memory-limit) of the app to 2GB (you can reset back the memory limit after plugin installation).
  Open a [Web Terminal](/apps/#web-terminal) and run the following:

```
    # cd /app/data/plugins/<plugin>
    # composer install --no-dev
    # chown -R www-data:www-data .
```

### Enabling PGP support

The Enigma plugin can be used to enable PGP support. The Enigma plugin is part of the
roundcube code and no installation is required. To enable the plugin:

* Add the following lines to `/app/data/customconfig.php`:

```
    array_push($config['plugins'], 'enigma');
    $config['enigma_pgp_homedir'] = '/app/data/enigma';
```

* Create the directory where enigma will save the PGP keys on the server:

```
    mkdir /app/data/enigma
    chown www-data:www-data /app/data/enigma
```
* New PGP keys can be created or existing ones can be imported in `Settings` -> `PGP Keys`

<center>
<img src="/img/roundcube-pgp-settings.png" class="shadow" width="600px">
</center>


* When composing new mail, you will see an Encryption icon in the tool bar.

<center>
<img src="/img/roundcube-encryption-icon.png" class="shadow" width="600px">
</center>

## Changing the title

Add the following lines to `/app/data/customconfig.php` using the [File Manager](/apps/#file-manager):

```
$rcmail_config['product_name'] = 'My Hosting Company';
```

## Skins

[Skins](https://plugins.roundcube.net/explore/) can be installed as follows:

* Extract the skin using the [File Manager](/apps#file-manager) into
  `/app/data/skins`.

* Change the ownership of the extracted skin to `www-data`.

* Set the new skin as the default skin by adding this line in `/app/data/customconfig.php`:

```
    $rcmail_config['skin'] = 'newskin_directory_name';
```

### Customizing CSS and logo

To customize CSS and logo, it's best to create a copy of an existing skin and make
changes as needed.

* Open a [Web terminal](/apps#web-terminal):

```
# cd /app/data/skins
# cp -Lr larry customskin
... make changes in customskin ...
# chown -R www-data:www-data customskin
```

* Set the new skin as the default skin by adding this line in `/app/data/customconfig.php`:

```
$rcmail_config['skin'] = 'customskin';
```

## Search

By default, the search field only searches the current folder. To search across all folders,
change the `Scope`.

<center>
<img src="/img/roundcube-search.png" class="shadow" width="500px">
</center>

