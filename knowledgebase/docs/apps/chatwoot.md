# <img src="/img/chatwoot-logo.png" width="25px"> Chatwoot App

## About

Chatwoot is an open-source customer engagement suite, an alternative to Intercom, Zendesk, Salesforce Service Cloud etc.

* Questions? Ask in the [Cloudron Forum - Chatwoot](https://forum.cloudron.io/category/135/chatwoot)
* [Chatwoot Website](https://www.chatwoot.com/)
* [Chatwoot docs](https://www.chatwoot.com/help-center/)
* [Chatwoot discussions](https://github.com/chatwoot/chatwoot/discussions)
* [Chatwoot issue tracker](https://github.com/chatwoot/chatwoot/issues)
* [Chatwoot Discord](https://discord.gg/cJXdrwS)

## Custom config

[Custom environment variables](https://www.chatwoot.com/docs/self-hosted/configuration/environment-variables)
can be set in `/app/data/env` using the [File manager](/apps/#file-manager). Be sure to reboot the app
after making any changes.

## Rails Console

To access the Chatwoot admin console, use the [Web terminal](/apps/#web-terminal) and source the environment
first before running rails commands.

```
# set -o allexport
# source /app/data/env
# RAILS_ENV=production bundle exec rails c
```

