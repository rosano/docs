FROM cloudron/base:3.2.0@sha256:ba1d566164a67c266782545ea9809dc611c4152e27686fd14060332dd88263ea

# passed from ./update.sh
ARG MKDOCS_MATERIAL_VERSION
ARG MKDOCS_REDIRECTS_VERSION
ARG SURFER_VERSION

RUN apt-get update && \
    apt install python3-setuptools && \
    pip3 install mkdocs-material==$MKDOCS_MATERIAL_VERSION && \
    pip3 install mkdocs-redirects==${MKDOCS_REDIRECTS_VERSION} && \
    npm i -g redoc-cli && \
    npm install -g cloudron-surfer@$SURFER_VERSION
